import React,{Component} from 'react';
import {connect} from 'react-redux'
import { Button } from 'reactstrap';
import {reduxCheck} from '../redux/actions/action'

class Component1 extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <div>
                 <Button onClick={()=>this.props.check(reduxCheck()) } color="danger">Danger!</Button> 
            </div>
        );
    }
}


const mapDispatchToProps=(dispatch)=>{
    return {
        check:dispatch
    }
}

export default connect(null,mapDispatchToProps)(Component1)