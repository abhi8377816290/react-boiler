import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { Button } from 'reactstrap';
import Component1 from './components/component1'
import Component2 from './components/component2'

function App() {
  return (
    <>
   <Component1/>

   <Component2/>
   </>
  );
}

export default App;
